import React from "react";
import {FlexBox, StyledInput } from '../Styled/styled';
import styled from "styled-components";

const FilterInput = styled(StyledInput)`
`;

const Filter = (props) => {
  const comment = props.comment;
  return (
    <div style={{position: 'relative', marginTop: 10, marginBottom: 20, background: '#fff'}}>
      <i className="fas fa-search" style={{
        position: 'absolute',
        top: 7,
        left: 9,
         color: '#d3d3d3'}}></i>
      <FilterInput placeholder="Filter" onChange={props.onChange} type='text' style={{paddingLeft: 35}} />
    </div>
  )
}

export default Filter;

