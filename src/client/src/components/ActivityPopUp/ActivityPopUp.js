import React from "react";
import {FlexBox, StyledImage } from '../Styled/styled';
import Popup from 'reactjs-popup';
import moment from 'moment';


const ActivityPopUp = (props) => {
  const {popup} = props;
  return (
    <Popup
    open={popup.open}
    closeOnDocumentClick
    onClose={popup.closeModal}>
      <FlexBox className="modal" justifyContent="space-between">
      <FlexBox>
        <StyledImage 
          src={popup.lastUserActivity.gravatar} 
          alt={popup.lastUserActivity.email}
          style={{cursor: 'pointer'}}/>
        <FlexBox className="activity" flexDirection="column">
          <div className="activity-email"> { popup.lastUserActivity.email } </div>
          <div className="activity-time"> Last activity: { moment(popup.lastUserActivity.timestamp).calendar() } </div> 
        </FlexBox>
      </FlexBox>
        <i className="far fa-window-close" onClick={popup.closeModal} style={{cursor: "pointer", color: "#333"}}></i>
      </FlexBox>
  </Popup>
  )
}

export default ActivityPopUp;
