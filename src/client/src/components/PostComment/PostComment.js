import React from "react";
import {FlexBox, StyledButton, StyledInput, StyledTextArea } from '../Styled/styled';
import styled from "styled-components";
import {postComment} from '../../api';

const StyledComment = styled.div`
  * {
      font-family: "Roboto";
  }
  display:flex;
  flex-direction:column;
  padding: 25px;
  background: #efefef;
`;

export default class PostComment extends React.Component {
  state = {
    errors: [],
    comment: {
      email: "",
      text: ""
    }
  }

  handleInputOnChnage = (value, inputName) => {
    let errors = [];
    let comment = {}
    switch (inputName) {
      case "email":
        comment = {email: value, text: this.state.comment.text };
        errors = this.validateComment(comment);
        this.setState({comment, errors});
        break;
      case "text":
        comment = {email: this.state.comment.email, text: value};
        errors = this.validateComment(comment);
        this.setState({comment, errors});
        break;
      default: break;
    }
  }

  validateComment = (comment) => {
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const errors = [];
    if(!comment.email || !emailRegex.test(comment.email)){
        errors.push("Please enter a valid email address.");
    }
    if(!comment.text){
      errors.push("Please enter a comment.");
    }
    return errors;
  }

  sumbmit = (comment) => {
    // unhandled errors check.
    const errors = this.validateComment(comment);
    if(errors.length != 0){
      return this.setState({errors});
    }
    postComment(comment).then(comments => this.props.addComment(comments[0]) );
  }

  render(){
    return (
    <StyledComment flexDirection="column" style={{padding:25, ...this.props.style }}>
      <StyledInput 
        type="text" 
        value={this.state.comment.email}
        placeholder="Email"
        onChange={({target})=>this.handleInputOnChnage(target.value,"email")}/>
        <br/>
      <StyledTextArea 
        style={{height:150}} 
        value={this.state.comment.text}
        placeholder="Message"
        onChange={({target})=>this.handleInputOnChnage(target.value,"text")}/>
      <StyledButton
        style={{width: 100, alignSelf: 'flex-end', marginTop:10}} 
        onClick={() => this.sumbmit(this.state.comment)}>Submit</StyledButton>
        <ul className="error-list">
          {
            this.state.errors.map(error => <li key={error} >{error}</li>)
          }
        </ul>
    </StyledComment>);
  }
}


