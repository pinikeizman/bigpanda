// *** dependencies *** //

const path = require("path");
const gulp = require("gulp");
const jshint = require("gulp-jshint");
const jscs = require("gulp-jscs");
const runSequence = require("run-sequence");
const nodemon = require("gulp-nodemon");
const plumber = require("gulp-plumber");
const notify = require("gulp-notify");
const server = require("tiny-lr")();

// *** config *** //

const paths = {
  scripts: [
    path.join("src", "server", "**", "*.js"),
  ],
  styles: [path.join("src", "client", "css", "*.css")],
  views: [
    path.join("src", "server", "**", "*.html"),
    path.join("src", "server", "*.html")
  ],
  server: path.join("src", "server", "server.js")
};

const lrPort = 35729;

const nodemonConfig = {
  script: paths.server,
  ext: "html js css",
  ignore: ["node_modules"],
  env: {
    NODE_ENV: "development",
    MONGO_USER: 'user',
    MONGO_PASSWORD: 'password',
    SCHEME_NAME: 'scheme',
    HOST: 'host:portz'
  }
};

// *** default task *** //

gulp.task("default", () => {
  runSequence(
    ["jshint"],
    // ["jscs"],
    ["lr"],
    ["nodemon"],
    ["watch"]
  );
});

// *** sub tasks ** //
gulp.task("jshint", () => {
  return gulp
    .src(paths.scripts)
    .pipe(plumber())
    .pipe(
      jshint({
        esnext: true
      })
    )
    .pipe(jshint.reporter("jshint-stylish"))
    .pipe(jshint.reporter("fail"))
    .pipe(
      notify({
        message: "jshint done"
      })
    );
});

gulp.task("jscs", () => {
  return gulp
    .src(paths.scripts)
    .pipe(plumber())
    .pipe(jscs())
    .pipe(jscs.reporter())
    .pipe(jscs.reporter("fail"))
    .pipe(
      notify({
        message: "jscs done"
      })
    );
});

gulp.task("styles", () => {
  return gulp.src(paths.styles).pipe(plumber());
});

gulp.task("views", () => {
  return gulp.src(paths.views).pipe(plumber());
});

gulp.task("lr", () => {
  server.listen(lrPort, err => {
    if (err) {
      return console.error(err);
    }
  });
});

gulp.task("nodemon", () => {
  return nodemon(nodemonConfig);
});

gulp.task("watch", () => {
  gulp.watch(paths.scripts, ["jshint", "jscs"]);
});
