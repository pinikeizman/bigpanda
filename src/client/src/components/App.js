import React from 'react';
import CommentFeed from './CommentFeed/CommentFeed';
import {getComments, getActivity} from './../api';

export default class App extends React.Component{
  state = {}

  componentDidMount(){
    getComments().then(comments => this.setState({comments}));
  }

  render(){
    const {comments} = this.state;
    return( 
        <CommentFeed comments={this.state.comments || []} />)
  }
};

