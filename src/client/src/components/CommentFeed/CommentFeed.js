import React from 'react';
import { FlexBox, StyledImage } from '../Styled/styled';
import Comment from '../Comment/Comment';
import PostComment from '../PostComment/PostComment';
import {getComments, getActivity} from '../../api';
import Filter from '../Filter/Filter';
import ActivityPopUp from '../ActivityPopUp/ActivityPopUp';
import moment from 'moment';

export default class CommentFeed extends React.Component{
  state = {
    comments: this.props.comments,
    popup: {
      open: false,
      lastUserActivity: {}
    }
  };

  // add comment to state
  addComment = (comment) => this.setState((prevState)=>({comments: [comment, ...this.state.comments]}));

  // close activity popup handler
  closeModal = () => {
    this.setState({popup:{open: false, lastUserActivity: {} }})
  }
  // fetch and the user last activity combine it with user gravatar and update the state
  getLastActivityAndOpenPopUp = (email) => {
    getActivity(email).then(lastUserActivity => {
      lastUserActivity.gravatar = (this.state.comments.find(comment => comment.email == lastUserActivity.email) ||{}).gravatar;
      this.setState({popup:{open: true, lastUserActivity}});
    });
  };

  // use lifecycle listen for first comment update from parent ajax container
  componentDidUpdate(prevProps, prevState, snapshot){
    if(this.state.comments.length == 0){
      this.setState({
        comments: this.props.comments
      })
    }
  }

  render(){
    const {filterText, comments, popup} = this.state;
    return( 
        <FlexBox flexDirection='column' style={{padding:50, width:'50%', margin: '0 auto' }}>
            <PostComment addComment={this.addComment} style={{ borderTopLeftRadius: 5, borderTopRightRadius:  5 }}/>
          <div style={{border: '1px solid #efefef', background: '#fff', padding:10, borderBottomLeftRadius: 5, borderBottomRightRadius: 5}}>
            <Filter onChange={ ({target}) => this.setState({ filterText: target.value }) }/>
            {
              comments
                .filter(comment => !filterText || comment.email.indexOf(filterText) != -1 )
                .sort( (commentA, commentB) =>{
                  return moment.utc(commentB.timestamp).diff(moment.utc(commentA.timestamp))
                  })
                .map(comment => <Comment onClick={this.getLastActivityAndOpenPopUp} key={comment._id} comment={comment}/> )
            }
          </div>
          <ActivityPopUp popup={{
            ...popup,
            closeModal: this.closeModal
          }}/>
      </FlexBox>)
  }
};

