import React from 'react';
import ReactDOM from 'react-dom';
import PostComment from './PostComment';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<PostComment/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
