const express = require('express');
const _ = require("lodash")
const router = express.Router();

const commentController = require('../controllers/comments');

// comment validation 
const validateComment = (comment) => { 
  const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  const errors = [];
  if(!comment.email || !emailRegex.test(comment.email)){
    errors.push({email: "Please enter a valid email address."});
  }
  if(!comment.text){
    errors.push({text:"Comment text length must be at least one."});
  }
  return errors;
};
// comments validation 
const validateComments = (comments) => { 
  return comments.reduce( (errors, comment, index) => {
    const commentErrors =  validateComment(comment);
    if( commentErrors.length != 0){ 
      commentErrors.push("comment num " + index);
      return [...errors, commentErrors]; 
    };
    return [...errors];
  }, []);

};

router.get('/activity', function (req, res, next) {
    const email = req.query.email;
    commentController
      .getActivity(email)
      .then(data => res.send(data)).catch(err => res.status(404).send({}));
});

// get all comment or get first last  activity query
router.get('/comments', function (req, res, next) {
  commentController.getAllComments()
    .then(comments => res.send(comments)).catch(err=> res.status(500).send(err));
});

router.post('/comments',function (req, res, next) {
  let comments = req.body;
  console.log(comments);
  if(!_.isArray(comments)){
    comments = [comments];
  }
  const errors = validateComments(comments);
  if(errors.length != 0){
    // response with bad request
    return res.status(400).send(errors);
  }
  // add comment
  commentController.addComment(comments)
    .then(comments => res.send(comments))
    .catch(err => res.status(404).send(err));
});

if(process.env.NODE_ENV == "development"){
  // delete all comments 
  // dev only.
  router.delete('/comments', function (req, res, next) {
    commentController
      .removeAll()
      .then(data => res.send(data))
      .catch(e => res.status(500).send("ouch.."));
  });
}

module.exports = router;
