import * as React from "react";
import {FlexBox, StyledImage } from '../Styled/styled';
import styled from "styled-components";
import  moment from 'moment';

const StyledComment = styled(FlexBox)`
  * {
      font-family: "Roboto";
  }
  padding:5px;
  font-size: 14px;
  background: #fff;
`;

const Comment = (props) => {
  const comment = props.comment;
  return (
  <StyledComment>
    <StyledImage 
      onClick={()=>props.onClick(props.comment.email)}
      src={comment.gravatar} 
      alt={comment.email}
      style={{cursor: 'pointer'}}/>
    <FlexBox flexDirection="column" style={{padding:5, paddingLeft: 10}}>
      <div style={{fontWeight: 'bold', color: '#515151'}}>
        {comment.email}
        <span style={{fontSize: 10, marginLeft: 10, color: '#555'}}>{moment(comment.timestamp).calendar()}</span>
      </div>
      <div style={{marginTop: 10, color: '#959595'}}>{comment.text}</div>
    </FlexBox>
  </StyledComment>)
}

export default Comment;
