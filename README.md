# BigPanda

# How do I get set up?
-   Clone this repository using git: `git clone https://bitbucket.org/pinikeizman/bigpanda.git`
-   Inside /bigpanda run `npm i` command.
-   Add your mongo config inside packge.json under scripts start.
-   Run npm start to lunch the Node Express server on port 5000.
-   Browse into: http://localhost:5000/ to see the CommentsFeed app.

# What is this repository for?
-   This repository is a project that solves Bigpanda Fullstack Developer interview challenge.
    The project build from Node Express app located under /bigpanda/src/server, Single page React.js app located under /bigpande/src/client and mongodb server as persistence layer.
-   Swagger api descriptor file can be found under /bigpanda dir with api specifications.
-   The React app implementation is a comment feed which cunsome the express api and have 3 basic functionalities:
    -   Fetch and display add comments.
    -   Add new comment.
    -   Get last user activity time ( the last time the user post a comment).