import React from 'react';
import ReactDOM from 'react-dom';
import Comment from './Comment';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Comment comment={[{
    email: "pini1089@gmail.com",
    image: "test",
    timestamp: "123456",
    text: "test",
    _id: 1231
  }]}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
