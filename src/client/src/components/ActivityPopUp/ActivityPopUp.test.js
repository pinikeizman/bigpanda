import React from 'react';
import ReactDOM from 'react-dom';
import ActivityPopUp from './ActivityPopUp';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<ActivityPopUp popup={{
    open: true,
    closeModel: ()=>{},
    lastUserActivity: {},
  }}/>, div);
  ReactDOM.unmountComponentAtNode(div);
});
