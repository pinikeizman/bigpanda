const _ = require('lodash');
var mongoose = require('mongoose');
var crypto = require('crypto');
const moment = require('moment');
const commentType = "comment";

if(process.env.NODE_ENV == 'development'){
  mongoose.set('debug', true) // enable logging collection methods + arguments to the console
}
const user = process.env.MONGO_USER || "";
const password = process.env.MONGO_PASSWORD || "";
const schemeName = process.env.SCHEME_NAME || "";
const host =  process.env.HOST || "";

console.log('mongodb://admin:zubur!123@ds029585.mlab.com:29585/comments-db');
console.log('mongo connection: mongodb://'+ user +':'+ password +'@'+ host+'/' +schemeName);

mongoose.connect('mongodb://'+ user +':'+ password +'@'+ host+'/' +schemeName);

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log('Connected to mongodb.');
});

var commentSchema = new mongoose.Schema({
  email: String,
  timestamp: {
    type: Date,
    index: true
  },
  type: {
    type: String,
    default: commentType,
  },
  gravatar: String,
  text: String
});

var Comments = mongoose.model('Comment', commentSchema);

const getActivity = (email) => {
  return Comments.findOne({
    email,
    type: "comment"
  },{}, { sort: { timestamp : -1} }).then(comment => {
    console.log(comment);
    return({
      timestamp: comment.timestamp,
      email: comment.email
    })});
}

const removeAll = () => { 
  return Comments
    .deleteMany({});
}

const getAllComments = () => {
  return Comments.find().sort({
    timestamp: -1
  });
}

const addComment = (comments) => {
  var enhencedComment = comments.map(comment => {
    const hashedEmail = crypto.createHash('md5').update(comment.email).digest("hex");
    comment.gravatar = "https://s.gravatar.com/avatar/" + hashedEmail + "?s=80";
    comment.timestamp = moment().format();
    comment.type = commentType;
    return comment;
  });

  return Comments.collection.insert(enhencedComment)
    .then(response => response.ops).catch(err=>{
    console.log(err);
    return ["opps.."]
;  })
};

module.exports = {
  addComment,
  getAllComments,
  removeAll,
  getActivity
};
