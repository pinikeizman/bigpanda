const baseUrl = process.env.NODE_ENV == 'development' ? 'http://localhost:5000/' : '/';

/*
 * GET /comments
 * return all comments
 */
export const getComments = () => 
  fetch( baseUrl +'comments' )
    .then(response => {
      if(response.status != 200){
         throw new Error(response.body);
      }
      return response.json();
  });

/*
 * GET /activity?email=<userEmail>
 * return <userEmail> last activity.
 */
export const getActivity = (email) =>
  defaultFetch(baseUrl + "activity?email=" + email).then(response => {
    if (response.status != 200) {
      throw new Error(response.body); 
    } else {
      return response.json();
    }
  });


/*
 * POST /comments
 * add comment to feed.
 */
  export const postComment = (comment) =>
    defaultFetch( baseUrl + "comments ",{
      method: "POST",
      body: JSON.stringify(comment)
    }).then( response => {
      if( response.status != 200 ){
        throw new Error(response.body);
      }else{
        return response.json();
      }
  }); 
  
/*
 * Default featch http request to reuse.
 */
  export const defaultFetch = ( url, options ) => {
    const headers = {
      "Content-Type": "application/json",
      "Accept": "application/json",
    }
    return fetch(url, {headers, ...options, })
  }
